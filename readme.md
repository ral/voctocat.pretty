Voctocat Footprints
===================

KiCad footprints of the voctocat logo.

Original image by Blinry, taken from his [website][1].
Converted to KiCad format with the `svg2mod` [tool][2]
and slightly post-processed.

[1]: https://blinry.org/voctocat/
[2]: https://github.com/svg2mod/svg2mod
